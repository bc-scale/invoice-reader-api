import cv2
import numpy as np


def preprocessImage(imageData: str):
    """
    Preprocesses the given image string, returning a threshold of
    it.
    ---
    imageData : str
      A string representing the image to be read.
    ---
    return:
      The image threshold.
    """

    npimg = np.fromstring(imageData, np.uint8)
    image = cv2.imdecode(npimg, cv2.IMREAD_COLOR)
    img = cv2.medianBlur(image, 5)

    greyScale = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return cv2.adaptiveThreshold(greyScale, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 4)
