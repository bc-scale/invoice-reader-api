from flask import Flask, request
import pytesseract
from . import image_preprocessing as imgpp

app = Flask(__name__)


@app.route('/api/reader', methods=['POST'])
def reader() -> str:
    """
    Endpoint responsible for receiving an image and returning its
    text.
    ---
    post:
      description: Post the image file
      responses:
        200:
          content:
            text: str
    """

    if 'file' not in request.files or request.files['file'] is None:
        return "File required", 400

    file = request.files['file']
    image = imgpp.preprocessImage(file.read())

    config = r'-c tessedit_char_whitelist=\.,0123456789 --psm 6 outputbase digits'
    text = pytesseract.image_to_string(image, config=config)
    return text
